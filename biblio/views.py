# -*- coding utf-8 -*- 
from biblio.models import *
from tutoriel.shortcuts import render
from biblio.forms import *
from django.http import HttpResponseRedirect, HttpResponse
from django.core.context_processors import csrf
from django.template import RequestContext
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib import * 
from django.contrib.auth import login, authenticate, logout
from django.contrib.auth.decorators import permission_required
from django.contrib.auth.models import User
from django.contrib.auth.models import Group

def show_main(request):
	return render(request,
		      "biblio/main.html",
		      {})

def show_authors(request):
	return render(request,		
		      "biblio/authors.html", 
		      {"authors": Author.objects.order_by("lastname")})

def show_users(request):
	return render(request,		
		      "biblio/Utilisateurs.html", 
		      {"users": Client.objects.order_by("username")})

def show_books(request):
	return render(request,
		      "biblio/books.html",		
		      {"books": Book.objects.order_by("title")})

def show_author(request, pk):
	return render(request,		
		      "biblio/author.html",
		      {"author": Author.objects.get(pk=pk)})

def show_book(request,pk):
	return render(request,
		      "biblio/book.html",		
		      {"book": Book.objects.get(pk=pk)})		

def show_user(request, pk):
	return render(request,		
		      "biblio/Utilisateur.html",
		      {"client": Client.objects.get(pk=pk)})

def show_subjects(request):
	return render(request,
		      "biblio/subjects.html",		
		      {"subjects": Subject.objects.order_by("label")})

def saisie_pret(request, pk):
	pretForm = PretForm()
	con ={'pretForm': pretForm} #Contexte pour le template ( on cré le form ici)
	con.update(csrf(request)) #empeche l'injection de js
	if len(request.POST) > 0:
		pretForm =PretForm(request.POST)
		con ={'pretForm': pretForm}
		if pretForm.is_valid():   
			pret=pretForm.save(commit=False)
			con.update(csrf(request))
			pret.possesseur=Client.objects.get(pk=pk)
			pret.destinataire=Client.objects.get(pk=pk)
			#Client.objects.filter(pk=pk).liste_books.filter(title=request.REQUEST.title).update(disponnibility=False)
			pret.save()
			return HttpResponseRedirect("/utilisateurs/") 
		else: HttpResponseRedirect("/")
		
	else:
		return render(request,'biblio/pretForm.html', con)

def modif_compte(request,pk):
        
    #list_taches=Task.objects.all()
	signinform = SigninForm()
	con ={'signinform': signinform} #Contexte pour le template ( on cré le form ici)
	con.update(csrf(request)) #empeche l'injection de js
	if len(request.POST) > 0:
		signinform =SigninForm(request.POST)
		con = {'signinform': signinform}
		if signinform.is_valid(): 
			con.update(csrf(request))
			Client.objects.filter(pk=pk).update(username=request.REQUEST.get('username', None))
			Client.objects.filter(pk=pk).update(password=request.REQUEST.get('password', None))
			Client.objects.filter(pk=pk).update(firstname=request.REQUEST.get('firstname', None))
			Client.objects.filter(pk=pk).update(naissance=request.REQUEST.get('naissance', None))
			Client.objects.filter(pk=pk).update(mail=request.REQUEST.get('mail', None))
			Client.objects.filter(pk=pk).update(adresse=request.REQUEST.get('adresse', None))
			Client.objects.filter(pk=pk).update(codepostal=request.REQUEST.get('codepostal', None))
			Client.objects.filter(pk=pk).update(ville=request.REQUEST.get('ville', None)) 
			
			#Client.objects.filter(pk=pk).update(liste_books=request.REQUEST.get('liste_books', None)) 
			return HttpResponseRedirect("/")
        #else:
        #    return render(request,'biblio/signin.html', con)
	else:      
		return render(request,'biblio/mod.html', con)


def signin(request):
    #list_taches=Task.objects.all()
	signinform = SigninForm()
	con ={'signinform': signinform} #Contexte pour le template ( on cré le form ici)
	con.update(csrf(request)) #empeche l'injection de js
	if len(request.POST) > 0:
		signinform =SigninForm(request.POST)
		con = {'signinform': signinform}
		if signinform.is_valid(): 
			con.update(csrf(request))
	        #user=User.objects.create_user(username=request.POST["username"],password=request.POST["password"])
            #user.save()
			userName = request.REQUEST.get('username', None)
			userPass = request.REQUEST.get('password', None)
			userMail = request.REQUEST.get('email', None)
			user = User.objects.create_user(userName, userMail, userPass)
			userGroup = Group.objects.get(name='Member') 
			userGroup.user_set.add(user.pk)
			user.save()
			signinform.save()
			return HttpResponseRedirect("/")
        #else:
        #    return render(request,'biblio/signin.html', con)
	else:      
		return render(request,'biblio/signin.html', con)



def saisie_Author(request):
    #list_taches=Task.objects.all()
	authorform = AuthorForm()
	con ={'authorform': authorform} #Contexte pour le template ( on cré le form ici)
	con.update(csrf(request)) #empeche l'injection de js
	if len(request.POST) > 0:
		authorform =AuthorForm(request.POST)
		con = {'authorform': authorform}
		if authorform.is_valid():   
			author=authorform.save(commit=False)
			con.update(csrf(request))
			author.save()
			return HttpResponseRedirect("/authors/")
	else:      
		return render(request,'biblio/AuthorForm.html', con)
        

def saisie_Book(request):
    #list_taches=Task.objects.all()
	bookForm = BookForm()
	con ={'bookForm': bookForm} #Contexte pour le template ( on cré le form ici)
	con.update(csrf(request)) #empeche l'injection de js
	if len(request.POST) > 0:
		bookForm =BookForm(request.POST)
		con = {'bookForm': bookForm}
		if bookForm.is_valid(): 
			con.update(csrf(request))			  
			bookForm.save()
			return HttpResponseRedirect("/books/")
	
        
	else:      
		return render(request,'biblio/BookForm.html', con)




def saisie_Sujet(request):
	sujetForm = SujetForm()
	con ={'sujetForm': sujetForm} #Contexte pour le template ( on cré le form ici)
	con.update(csrf(request)) #empeche l'injection de js
	if len(request.POST) > 0:
		sujetForm =SujetForm(request.POST)
		con ={'sujetForm': sujetForm}
		if sujetForm.is_valid():   
			sujet=sujetForm.save(commit=False)
			con.update(csrf(request))
			sujet.save()
			return HttpResponseRedirect("/subjects/") 
	else:      
		return render(request,'biblio/SujetForm.html', con)


def login_page(request):
	context = {}
	context.update(csrf(request))
	if request.method== "POST":
		form = LoginForm(request.POST)
		if form.is_valid():
			username = form.cleaned_data["username"]
			password = form.cleaned_data["password"]
			user= authenticate(username=username,password=password)
			if user is None:
				context["errmsg"] = "Echec d'autentification"
			elif user.is_active:
				login(request, user)
				url = request.GET.get("next", "/")
				return	HttpResponseRedirect(url)
			else:
				context["errmsg"] = "Compte désactivé"	
		else:
			context["errmsg"] = "Paramètre non valid"		
	else:
		form = LoginForm()
	context["form"]	= form
	return render(request, 'biblio/login.html', context)


def input_action(request):
	if request.user.is_authenticated():
		logout(request)
	return HttpResponseRedirect("/")



def get_author_json(request):
	author=Author.objects.get(pk=pk)
	data = {"name": author.lastname, "fname": author.firtsname}
	import json
	text = json.damps(data, indent=2, ensure_ascii=False)
	response = HttpResponse(text, mimetype='application/json; charset=utf8')
	return response


@permission_required('biblio.can_edit_database')
def author_add(request):
	if request.method=="POST":
		form = Author_form(request.POST)
		if form.is_valid():
			author = form.save()
			return HttpResponseRedirect("/author/%$" % author.id)
	else:
		form = Author_form()
	context = {"form": form}
	context.update(csrf(request))
	return render(request,"biblio/author-add.html",	context)
