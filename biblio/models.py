from django.db import models

# Create your models here.

class Author(models.Model):
	lastname = models.CharField(max_length = 50)
	firstname = models.CharField(max_length = 50)

	def __str__(self):
		return "%s %s" %(self.firstname,self.lastname)

class Book(models.Model):
	title = models.CharField(max_length = 50)
	authors = models.ManyToManyField("Author", related_name = "books")
	subject = models.ForeignKey("Subject")
	disponnibility = models.BooleanField()

	def __str__(self):
		return "%s" %(self.title)

class Subject(models.Model):
	label = models.CharField(max_length = 50)
	
	def __str__(self):
		return self.label
		
class Client(models.Model):
	username = models.CharField(max_length = 50)
	password = models.CharField(max_length = 50)
	lastname = models.CharField(max_length = 50)	
	firstname = models.CharField(max_length = 50)
	naissance = models.DateField(max_length = 10)
	mail = models.EmailField(max_length = 70)
	adresse = models.CharField(max_length = 50)
	codepostal = models.CharField(max_length = 5)
	ville = models.CharField(max_length = 50)
	liste_books = models.ManyToManyField("Book",related_name ="owner")	
	liste_pret = models.ManyToManyField("Pret", related_name = "holder")
	
	def __str__(self):
		return "%s" %self.username

class Pret(models.Model):
	possesseur = models.ForeignKey(Client, related_name="preteur")
	destinataire = models.ForeignKey(Client,  related_name="emptrunteur")
	titre = models.ForeignKey("Book")
	debut = models.DateTimeField(auto_now_add=True, auto_now=True)
	duree = models.CharField(max_length = 2)
	fin = models.DateTimeField(auto_now_add=True, auto_now=False, verbose_name="Fin du pret")

	def __str__(self):
		return "%s" %(self.title)
		
class User(models.Model):
	username = models.CharField(max_length = 50)
	password = models.CharField(max_length = 50)

	def __str__(self):
		return "%s" %(self.username)

