from django.contrib import admin
from biblio.models import *

admin.site.register(Author)
admin.site.register(Book)
admin.site.register(Subject)
admin.site.register(Client)
admin.site.register(User)
