from tastypie.resources import modelResources
from biblio.models import *

class AuthorResource(modelResources):
	class Meta:
		queryset = Author.objects.all()
		resource_name = 'author'
