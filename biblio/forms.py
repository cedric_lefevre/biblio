from django.forms import ModelForm, CharField, Textarea, Form, PasswordInput
from django import forms
from biblio.models import *
from django.contrib.auth.models import User




class LoginForm(Form):
	username = CharField(max_length=100, label="Identifiant")
	password = CharField(max_length=100, label="Mot de Passe", widget=PasswordInput)




class BookForm(ModelForm):
	def __init__(self, *args, **kwargs):
		super(BookForm, self).__init__(*args, **kwargs)
		self.fields['title'].label = "Titre"
		self.fields['authors'].label="Auteurs"
		self.fields['authors'].widget= forms.widgets.CheckboxSelectMultiple()
		self.fields["authors"].help_text = ""
		self.fields["authors"].queryset = Author.objects.all()
		self.fields['subject'].label="Genre"
		self.fields['disponnibility'].label="Est Disponnible"
		
	class Meta:
		model = Book
		fields = ('title', 'authors', 'subject', 'disponnibility')
		
class AuthorForm(ModelForm):
	def __init__(self, *args, **kwargs):
		super(AuthorForm, self).__init__(*args, **kwargs)
		self.fields['lastname'].label = "nom"
		self.fields['firstname'].label="prénom"
		
	class Meta:
		model = Author
		fields = ('firstname', 'lastname')



class SujetForm(ModelForm):
	def __init__(self, *args, **kwargs):
		super(SujetForm, self).__init__(*args, **kwargs)
		self.fields['label'].label = "Titre du sujet"
		
	class Meta:
		model = Subject
		fields =  ('label',)

class SigninForm(ModelForm):
	def __init__(self, *args, **kwargs):
		super(SigninForm, self).__init__(*args, **kwargs)
		self.fields['username'].label = "identifiant"
		self.fields['password'].label = "mot de passe"
		self.fields['lastname'].label = "prenom"
		self.fields['firstname'].label = "nom"
		self.fields['naissance'].label = "naissance"
		self.fields['mail'].label = "mail"
		self.fields['adresse'].label = "adresse"
		self.fields['codepostal'].label = "codepostal"
		self.fields['ville'].label = "ville"
		self.fields["liste_books"].queryset = Book.objects.all()
	class Meta:
		model = Client
		fields =  ('username', 'password', 'lastname', 'firstname', 'naissance', 'mail', 'adresse', 'codepostal', 'ville', 'liste_books')

class PretForm(ModelForm):
	def __init__(self, *args, **kwargs):
		super(PretForm, self).__init__(*args, **kwargs)
		self.fields['titre'].queryset = Book.objects.all()
		self.fields['duree'].label="durée du prêt"
	class Meta:
		model = Pret
		fields =  ( 'titre', 'duree')
	
