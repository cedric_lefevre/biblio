from django.conf.urls import patterns, include, url
#from biblio.api import author_resource
# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'tutoriel.views.home', name='home'),
    # url(r'^tutoriel/', include('tutoriel.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),
    
    
    url(r'^/?$' , "biblio.views.show_main"),
    url(r'^authors/?$' , "biblio.views.show_authors"),
    url(r'^books/?$' , "biblio.views.show_books"),
    url(r'^subjects/?$' , "biblio.views.show_subjects"),
    url(r'^utilisateurs/?$' , "biblio.views.show_users"),
    
    url(r'^utilisateur/(\d+)/?$' , "biblio.views.show_user"),
    url(r'^book/(\d+)?$' , "biblio.views.show_book"),
    url(r'^author/(\d+)?$' , "biblio.views.show_author"),
    
    
    url(r'^saisieauthor/?$' ,"biblio.views.saisie_Author"),
    url(r'^saisiebook/?$' ,"biblio.views.saisie_Book"),
    url(r'^saisiesujet/?$' ,"biblio.views.saisie_Sujet"),
    url(r'^ajoutpret/(\d)?$' ,"biblio.views.saisie_pret"),    
    
    url(r'^signin/?$' ,"biblio.views.signin"),
    url(r'^login/?$' ,"biblio.views.login_page"),
    url(r'^logout/?$' ,"biblio.views.input_action"),    
    
    url(r'^author_add/(\d+)/?$' , "biblio.views.author_add"),
    url(r'^get_author_json/(\d+)$' , "biblio.views.get_author_json"),
    
    url(r'^modif_compte/(\d+)$' , "biblio.views.modif_compte"),
   
    #url(r'^api/?$' ,include(author_resource.uls)),
 
)

